#!/bin/sh

REPO=https://gitlab.com/jessicalucas/conchayoro.git
NODE1=ip172-18-0-30-blnekk8nddr0008imcog@direct.labs.play-with-docker.com
NODE2=ip172-18-0-40-blnekk8nddr0008imcog@direct.labs.play-with-docker.com
NODE3=ip172-18-0-41-blnekk8nddr0008imcog@direct.labs.play-with-docker.com
NODE4=ip172-18-0-15-blnekk8nddr0008imcog@direct.labs.play-with-docker.com

echo "Configuração do ambiente remoto"
  
ssh -t $NODE1 "git clone $REPO && cd conchayoro/ambiente && docker-compose --compatibility up -d jenkins"

ssh -t $NODE2 "git clone $REPO && cd conchayoro/ambiente && docker-compose --compatibility up -d nexus && docker-compose exec nexus sh /deploy-config/nexus/init.sh"

ssh -t $NODE3 "git clone $REPO && cd conchayoro/ambiente && docker-compose --compatibility up -d sonar" 
   
echo "Configuração do ambiente remoto concluída com sucesso!!"